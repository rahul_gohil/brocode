import React, { Component } from 'react';
import * as providerList from './providerList.json'
class Main extends Component {

  render() {
    let comp
    let price1
    const account1 = this.props.account
    const hasValue = Object.keys(providerList["default"]).includes(account1)
    if (hasValue) {
      const name = providerList["default"][account1]["name"]
      const price =   window.web3.utils.toWei(providerList["default"][account1]["price"].toString(), 'Ether')
      price1 = price
      this.props.createProduct(name, price)
    }

    return (
      <div id="content"> 
        <h2>Acquire Subsidy</h2>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Name</th>
              <th scope="col">Price</th>
              <th scope="col">Owner</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody id="productList">
            { this.props.products.map((product, key) => {
              return(
                <tr key={key}>
                  <th scope="row">{product.id.toString()}</th>
                  <td>{product.name}</td>
                  <td>{15250 * window.web3.utils.fromWei(product.price.toString(), 'Ether')} Rupees</td>
                  <td>{product.owner}</td>
                  <td>
                    { !product.purchased
                      ? <button
                          name={product.id}
                          value={product.price}
                          onClick={(event) => {
                            this.props.purchaseProduct(event.target.name, event.target.value)
                          }}
                        >
                          Acquire
                        </button>
                      : null
                    }
                    </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Main;